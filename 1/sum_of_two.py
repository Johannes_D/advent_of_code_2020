'''this is the first advent of code puzzle'''

def calc_product():
    '''this funtion calculates if the sum of two or three summands
    is 2020 and returns the product of the summands'''
    expenses = open('expenses.txt').read().splitlines()
    expenses = [int(i) for i in expenses]
    for summand_1 in expenses:
        for summand_2 in expenses:
            #if three summands are searched for
            for summand_3 in expenses:
                summ = summand_1 + summand_2 + summand_3
                # if summ < 2050:
                    # print(summ)
                if summ == 2020:
                    print('summand_1: ', summand_1, '\nsummand_2: ', summand_2,
                            '\nsummand_3: ', summand_3)
                    return summand_1 * summand_2 * summand_3

    print('something went wrong')
    return 1

solution = calc_product()
print('the solution is: ', solution)
