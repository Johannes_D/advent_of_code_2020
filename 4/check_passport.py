'''this is the fourth  advent of code puzzle'''
import re

def read_input_data():
    '''reding the input data'''
    passport_data_ = open('input.txt').read().split('\n\n')
    return passport_data_

def check_passport_vaidity(passport_data_):
    '''check if the necessary data is present'''
    number_of_valid_passports = 0
    for entry in passport_data_:
        sorted_ = re.split(' |\n', entry)

        # data complete
        if len(sorted_) == 8:
            number_of_valid_passports += 1

        # one enty is missing -> check if cid is missing, than it's OK
        if len(sorted_) == 7:
            number_of_valid_passports += 1
            for pair in sorted_:
                (key, value) = pair.split(':')
                if key == 'cid':
                    number_of_valid_passports -= 1
                    break

    return number_of_valid_passports

if __name__ == "__main__":
    passport_data = read_input_data()
    print(check_passport_vaidity(passport_data))
