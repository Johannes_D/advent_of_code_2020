'''this is the second advent of code puzzle'''
import re

def read_input_data():
    '''docstring'''
    data = {}
    with open('input.txt') as input_data:
        for counter, line in enumerate(input_data):
            (occurences, letter, password) = line.split()
            data[counter] = (occurences, letter, password)
    return data

def count_occurences(letter, password):
    '''counting letters in password'''
    count = 0
    for i in password:
        if i == letter:
            count = count + 1
    return count

def check_by_position(data_):
    '''checks if the position conditions are held'''
    valid_passwords_ = 0
    for i in data_:
        numbers = re.findall(r'\d+', data_[i][0])
        position_1 = int(numbers[0]) - 1
        position_2 = int(numbers[1]) - 1
        pw_letter_1 = data_[i][2][position_1]
        pw_letter_2 = data_[i][2][position_2]
        letter = data_ [i][1][0]
        if letter in pw_letter_1 and letter not in pw_letter_2:
            valid_passwords_ += 1
        if letter not in pw_letter_1 and letter in pw_letter_2:
            valid_passwords_ += 1

    return valid_passwords_


def check_valid_passwords(data_):
    '''cheks if the occurences conditions are held'''
    valid_passwords_ = 0
    for i in data_:
        occurences = count_occurences(data_[i][1][0],data_[i][2])
        # minium check
        numbers = re.findall(r'\d+', data_[i][0])
        minimum = int(numbers[0])
        maximum = int(numbers[1])
        if occurences >= minimum:
            if occurences <= maximum:
                valid_passwords_ += 1

    return valid_passwords_

if __name__ == "__main__":
    data_dict = read_input_data()
    print(check_valid_passwords(data_dict))
    print(check_by_position(data_dict))
