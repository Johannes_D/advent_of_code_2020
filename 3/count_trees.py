'''this is the third advent of code puzzle'''

def read_input_data():
    '''reding the input data'''
    return open('input.txt').read().splitlines()

def count_trees(tree_map_, sideways_slope=3, downwards_slope=1):
    '''count the trees ('#') by a 3 to the right and 1 down pattern'''
    position = 0
    max_position = 30
    line = 0
    max_line = 323
    tree_counter = 0

    while line < max_line:
        # print ('line: ', line, 'position: ', position, tree_map_[line][position])
        if tree_map_[line][position] == '#':
            tree_counter += 1
        line += downwards_slope
        position = position + sideways_slope
        if position > max_position:
            position = position - 31
    return tree_counter

if __name__ == "__main__":
    tree_map = read_input_data()
    # print(count_trees(tree_map,3,1))
    A = count_trees(tree_map,1,1)
    B = count_trees(tree_map,3,1)
    C = count_trees(tree_map,5,1)
    D = count_trees(tree_map,7,1)
    E = count_trees(tree_map,1,2)
    print('product of all slopes: ', A*B*C*D*E)
